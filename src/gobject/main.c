#include <config.h>

#define STREQ(a, b) (strcmp ((a), (b)) == 0)
#define STRNEQ(a, b) (! (STREQ (a, b)))

#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "gettext.h"
#include "relocatable.h"
#include "attribute.h"
#include <disfluid.h>
#include <disfluid-gobject.h>

#define _(String) dgettext (PACKAGE, (String))
#define N_(String) (String)

void
disfluid_gobject_empty (void)
{
}
