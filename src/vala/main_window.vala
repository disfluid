namespace Disfluid {
	class MainWindow: Adw.ApplicationWindow {
		private Adw.WindowTitle _window_title;
		private Gtk.Box _layout;

		private string? _subtitle = null;
		private Gtk.Widget? _disfluid_content = null;

		public string? subtitle {
			get {
				return this._subtitle;
			}
			set {
				this._subtitle = value;
				this._window_title.subtitle = value;
			}
		}

		public Gtk.Widget? disfluid_content {
			get {
				return this._disfluid_content;
			}
			set {
				if (this._disfluid_content != null) {
					this._layout.remove (this._disfluid_content);
				}
				this._disfluid_content = value;
				if (value != null) {
					this._layout.append (value);
				}
			}
		}

		construct {
			this._window_title = new Adw.WindowTitle (Disfluid.metaphor_name (), this._subtitle);
			var bar = new Adw.HeaderBar ();
			bar.centering_policy = Adw.CenteringPolicy.STRICT;
			bar.title_widget = this._window_title;
			this._layout = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
			this._layout.append (bar);
			if (this._disfluid_content != null) {
				this._layout.append (this._disfluid_content);
			}
			this.content = this._layout;
		}

		public MainWindow (Gtk.Widget? content) {
			Object (subtitle: null, disfluid_content: content);
		}

		public MainWindow.specific (string subtitle, Gtk.Widget? content) {
			Object (subtitle: subtitle, disfluid_content: content);
		}
	}
}
