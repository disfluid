int main (string[] args) {
	Intl.setlocale (LocaleCategory.ALL, "");
	var name = "eu.planete_kraus.Disfluid";
	if (Disfluid.is_nightly ()) {
		name += ".Devel";
	}
	var app = new Adw.Application(
		name,
		ApplicationFlags.FLAGS_NONE
		);
	app.activate.connect(() => {
			var window = Disfluid.make_about_window ();
			window.set_application (app);
			window.present();
		});
  return app.run(args);
}
