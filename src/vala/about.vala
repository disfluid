namespace Disfluid {
	Adw.AboutWindow make_about_window () {
		string appid = "eu.planete_kraus.Disfluid";
		if (Disfluid.is_nightly ()) {
			appid += ".Devel";
		}
		var window = new Adw.AboutWindow ();
		window.set_application_icon (appid);
		window.set_application_name (appid);
		window.set_version (Disfluid.version ());
		window.set_release_notes (Disfluid.whats_new ());
		window.set_release_notes_version (Disfluid.major_version ());
		window.set_website (Disfluid.website ());
		var n_authors = Disfluid.count_authors ();
		string[] developers = new string[0];
		string[] designers = new string[0];
		string[] artists = new string[0];
		string[] documenters = new string[0];
		for (size_t i = 0; i < n_authors; i++) {
			var email = Disfluid.author_email (i);
			var uri = Disfluid.author_uri (i);
			string full_name = Disfluid.author_name (i);
			if (email != null) {
				full_name += " <" + email + ">";
			} else if (uri != null) {
				full_name += " " + uri;
			}
			if (Disfluid.author_is_developer (i)) {
				developers += full_name;
			}
			if (Disfluid.author_is_designer (i)) {
				designers += full_name;
			}
			if (Disfluid.author_is_artist (i)) {
				artists += full_name;
			}
			if (Disfluid.author_is_documenter (i)) {
				documenters += full_name;
			}
		}
		window.set_developers (developers);
		window.set_designers (designers);
		window.set_artists (artists);
		window.set_documenters (documenters);
		var translation_credits = Disfluid.translation_credits ();
		window.set_translator_credits (translation_credits);
		window.set_copyright (Disfluid.copyright_statement ());
		window.set_license_type (Gtk.License.AGPL_3_0);
		return window;
	}
}
