#ifndef DISFLUID_TESTS_INCLUDED
# define DISFLUID_TESTS_INCLUDED

struct disfluid_tests_report;

static inline char *run_tests (size_t *n_tests, size_t *n_errors);

static inline int test_append_init (int *fd, char **filename);
static inline int test_append_hello (const char *filename, size_t *n_before,
				     size_t *n_after);
static inline int test_append_count (const char *filename, size_t *n);

# include <check.h>

# include "disfluid-init.h"
# include "disfluid-version.h"
# include "disfluid-cache-entry.h"
# include "disfluid-cache-entry-key.h"
# include "string-desc.h"
# include "disfluid-append-only-file.h"
# include "disfluid-trie.h"
# include "disfluid-cache-group.h"
# include "disfluid-activity-object.h"

# include <jansson.h>

# define BYTES * 1

# define KILOBYTES * 1024 BYTES

# define MEGABYTES * 1024 KILOBYTES

# define THOUSAND * 1000

# define MILLION THOUSAND THOUSAND

# define BILLION THOUSAND MILLION

/* *INDENT-OFF* */
START_TEST (test_check_version)
/* *INDENT-ON* */

{
  const char *lib_version = version ();
  ck_assert_str_eq (lib_version, VERSION);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_read_normal)
/* *INDENT-ON* */

{
  static const char *key_value = "GET http://example.com\r\n";
  static const char *header_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *body_value = "Hello, world!";
  const string_desc_t key = {._nbytes = strlen (key_value),._data =
      (char *) key_value
  };
  const string_desc_t header = {._nbytes = strlen (header_value),._data =
      (char *) header_value
  };
  const string_desc_t body = {._nbytes = strlen (body_value),._data =
      (char *) body_value
  };
  const struct timespec request_date = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date = {.tv_sec = 111213,.tv_nsec = 141516 };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  error =
    ao_cache_entry_push (file, &top, &request_date, &response_date, true, key,
			 header, body);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  string_desc_t check_key = { 0 };
  string_desc_t check_header = { 0 };
  string_desc_t check_body = { 0 };
  struct timespec check_request_date, check_response_date;
  bool check_invalidated;
  error =
    ao_cache_entry_read (file, top, &check_request_date, &check_response_date,
			 &check_invalidated, true, true, true, &check_key,
			 &check_header, &check_body);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (check_request_date.tv_sec, 12345);
  ck_assert_int_eq (check_request_date.tv_nsec, 678910);
  ck_assert_int_eq (check_response_date.tv_sec, 111213);
  ck_assert_int_eq (check_response_date.tv_nsec, 141516);
  ck_assert (check_invalidated);
  ck_assert_int_eq (check_key._nbytes, strlen (key_value));
  ck_assert_int_eq (check_header._nbytes, strlen (header_value));
  ck_assert_int_eq (check_body._nbytes, strlen (body_value));
  ck_assert_int_eq (memcmp (check_key._data, key_value, strlen (key_value)),
		    0);
  ck_assert_int_eq (memcmp
		    (check_header._data, header_value, strlen (header_value)),
		    0);
  ck_assert_int_eq (memcmp
		    (check_body._data, body_value, strlen (body_value)), 0);
  FREE (check_key._data);
  FREE (check_header._data);
  FREE (check_body._data);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_invalidate_cache_entry_noop)
/* *INDENT-ON* */

{
  /* Add a single cache entry to the file. It is already invalidated,
     so invalidating it again does not do anything. */
  static const char *key_value = "GET http://example.com\r\n";
  static const char *header_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *body_value = "Hello, world!";
  const string_desc_t key = {._nbytes = strlen (key_value),._data =
      (char *) key_value
  };
  const string_desc_t header = {._nbytes = strlen (header_value),._data =
      (char *) header_value
  };
  const string_desc_t body = {._nbytes = strlen (body_value),._data =
      (char *) body_value
  };
  const struct timespec request_date = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date = {.tv_sec = 111213,.tv_nsec = 141516 };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  error =
    /* Notice the "true" here. */
    ao_cache_entry_push (file, &top, &request_date, &response_date, true, key,
			 header, body);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  size_t invalidated_offset = 42;
  error = ao_cache_entry_invalidate (file, top, &invalidated_offset);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (invalidated_offset, top);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_invalidate_cache_entry)
/* *INDENT-ON* */

{
  /* Add a single cache entry to the file. It is not invalidated, so
     invalidating it again pushes a new cache entry. The cache entry
     reuses storage for the key, header and body, so it is just 80
     bytes after the existing entry. */
  static const char *key_value = "GET http://example.com\r\n";
  static const char *header_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *body_value = "Hello, world!";
  const string_desc_t key = {._nbytes = strlen (key_value),._data =
      (char *) key_value
  };
  const string_desc_t header = {._nbytes = strlen (header_value),._data =
      (char *) header_value
  };
  const string_desc_t body = {._nbytes = strlen (body_value),._data =
      (char *) body_value
  };
  const struct timespec request_date = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date = {.tv_sec = 111213,.tv_nsec = 141516 };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  error =
    /* Notice the "false" here. */
    ao_cache_entry_push (file, &top, &request_date, &response_date, false,
			 key, header, body);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  size_t invalidated_offset = 42;
  error = ao_cache_entry_invalidate (file, top, &invalidated_offset);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (invalidated_offset, top + 80);
  string_desc_t check_key = { 0 };
  string_desc_t check_header = { 0 };
  string_desc_t check_body = { 0 };
  struct timespec check_request_date, check_response_date;
  bool check_invalidated;
  error =
    ao_cache_entry_read (file, invalidated_offset, &check_request_date,
			 &check_response_date, &check_invalidated, true, true,
			 true, &check_key, &check_header, &check_body);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (check_request_date.tv_sec, 12345);
  ck_assert_int_eq (check_request_date.tv_nsec, 678910);
  ck_assert_int_eq (check_response_date.tv_sec, 111213);
  ck_assert_int_eq (check_response_date.tv_nsec, 141516);
  ck_assert (check_invalidated);
  ck_assert_int_eq (check_key._nbytes, strlen (key_value));
  ck_assert_int_eq (check_header._nbytes, strlen (header_value));
  ck_assert_int_eq (check_body._nbytes, strlen (body_value));
  ck_assert_int_eq (memcmp (check_key._data, key_value, strlen (key_value)),
		    0);
  ck_assert_int_eq (memcmp
		    (check_header._data, header_value, strlen (header_value)),
		    0);
  ck_assert_int_eq (memcmp
		    (check_body._data, body_value, strlen (body_value)), 0);
  FREE (check_key._data);
  FREE (check_header._data);
  FREE (check_body._data);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_invalidate_cache_group_noop)
/* *INDENT-ON* */

{
  /* There are 2 entries in the group. Both are already invalidated,
     so invalidating the group is a no-op. */
  static const char *key_1_value = "GET http://example.com\r\n";
  static const char *key_2_value = "GET http://example.com\r\nlater\r\n";
  static const char *header_1_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *header_2_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nLast-Modified: later\r\n\r\n";
  static const char *body_1_value = "Hello, world!";
  static const char *body_2_value = "Hello, world 2!";
  const string_desc_t key_1 = {._nbytes = strlen (key_1_value),._data =
      (char *) key_1_value
  };
  const string_desc_t key_2 = {._nbytes = strlen (key_2_value),._data =
      (char *) key_2_value
  };
  const string_desc_t header_1 = {._nbytes = strlen (header_1_value),._data =
      (char *) header_1_value
  };
  const string_desc_t header_2 = {._nbytes = strlen (header_2_value),._data =
      (char *) header_2_value
  };
  const string_desc_t body_1 = {._nbytes = strlen (body_1_value),._data =
      (char *) body_1_value
  };
  const string_desc_t body_2 = {._nbytes = strlen (body_2_value),._data =
      (char *) body_2_value
  };
  const struct timespec request_date_1 = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date_1 = {.tv_sec = 111213,.tv_nsec = 141516
  };
  const struct timespec request_date_2 = {.tv_sec = 22345,.tv_nsec = 678920 };
  const struct timespec response_date_2 = {.tv_sec = 222223,.tv_nsec = 242526
  };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  size_t offset_1, offset_2;
  error =
    /* Notice the "true" here. */
    ao_cache_entry_push (file, &offset_1, &request_date_1, &response_date_1,
			 true, key_1, header_1, body_1);
  ck_assert_int_eq (error, 0);
  error =
    /* Notice the "true" here too. */
    ao_cache_entry_push (file, &offset_2, &request_date_2, &response_date_2,
			 true, key_2, header_2, body_2);
  ck_assert_int_eq (error, 0);
  size_t offsets[] = { offset_1, offset_2 };
  error =
    ao_cache_group_push (file, &top, sizeof (offsets) / sizeof (offsets[0]),
			 offsets);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  size_t invalidated_offset = 42;
  error = ao_cache_group_invalidate (file, top, &invalidated_offset);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (invalidated_offset, top);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_invalidate_cache_group_partial)
/* *INDENT-ON* */

{
  /* There are 2 entries in the group. The first one is invalidated,
     but not the second one. */
  static const char *key_1_value = "GET http://example.com\r\n";
  static const char *key_2_value = "GET http://example.com\r\nlater\r\n";
  static const char *header_1_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *header_2_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nLast-Modified: later\r\n\r\n";
  static const char *body_1_value = "Hello, world!";
  static const char *body_2_value = "Hello, world 2!";
  const string_desc_t key_1 = {._nbytes = strlen (key_1_value),._data =
      (char *) key_1_value
  };
  const string_desc_t key_2 = {._nbytes = strlen (key_2_value),._data =
      (char *) key_2_value
  };
  const string_desc_t header_1 = {._nbytes = strlen (header_1_value),._data =
      (char *) header_1_value
  };
  const string_desc_t header_2 = {._nbytes = strlen (header_2_value),._data =
      (char *) header_2_value
  };
  const string_desc_t body_1 = {._nbytes = strlen (body_1_value),._data =
      (char *) body_1_value
  };
  const string_desc_t body_2 = {._nbytes = strlen (body_2_value),._data =
      (char *) body_2_value
  };
  const struct timespec request_date_1 = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date_1 = {.tv_sec = 111213,.tv_nsec = 141516
  };
  const struct timespec request_date_2 = {.tv_sec = 22345,.tv_nsec = 678920 };
  const struct timespec response_date_2 = {.tv_sec = 222223,.tv_nsec = 242526
  };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  size_t offset_1, offset_2;
  error =
    /* Notice the "true" here. */
    ao_cache_entry_push (file, &offset_1, &request_date_1, &response_date_1,
			 true, key_1, header_1, body_1);
  ck_assert_int_eq (error, 0);
  error =
    /* However, notice the "false" here. */
    ao_cache_entry_push (file, &offset_2, &request_date_2, &response_date_2,
			 false, key_2, header_2, body_2);
  ck_assert_int_eq (error, 0);
  size_t offsets[] = { offset_1, offset_2 };
  error =
    ao_cache_group_push (file, &top, sizeof (offsets) / sizeof (offsets[0]),
			 offsets);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  size_t invalidated_offset = 42;
  error = ao_cache_group_invalidate (file, top, &invalidated_offset);
  ck_assert_int_eq (error, 0);
  ck_assert_int_gt (invalidated_offset, top);
  size_t *final_offsets = NULL;
  size_t n_entries = 42;
  error =
    ao_cache_group_read (file, invalidated_offset, &n_entries,
			 &final_offsets);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (n_entries, 2);
  ck_assert_int_eq (final_offsets[0], offset_1);
  ck_assert_int_gt (final_offsets[1], offset_2);
  string_desc_t check_key = { 0 };
  string_desc_t check_header = { 0 };
  string_desc_t check_body = { 0 };
  struct timespec check_request_date, check_response_date;
  bool check_invalidated;
  error =
    ao_cache_entry_read (file, final_offsets[1], &check_request_date,
			 &check_response_date, &check_invalidated, true, true,
			 true, &check_key, &check_header, &check_body);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (check_request_date.tv_sec, 22345);
  ck_assert_int_eq (check_request_date.tv_nsec, 678920);
  ck_assert_int_eq (check_response_date.tv_sec, 222223);
  ck_assert_int_eq (check_response_date.tv_nsec, 242526);
  ck_assert (check_invalidated);
  ck_assert_int_eq (check_key._nbytes, strlen (key_2_value));
  ck_assert_int_eq (check_header._nbytes, strlen (header_2_value));
  ck_assert_int_eq (check_body._nbytes, strlen (body_2_value));
  ck_assert_int_eq (memcmp
		    (check_key._data, key_2_value, strlen (key_2_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_header._data, header_2_value,
		     strlen (header_2_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_body._data, body_2_value, strlen (body_2_value)),
		    0);
  FREE (check_key._data);
  FREE (check_header._data);
  FREE (check_body._data);
  FREE (final_offsets);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_invalidate_cache_group_all)
/* *INDENT-ON* */

{
  /* There are 2 entries in the group. The first one is invalidated,
     but not the second one. */
  static const char *key_1_value = "GET http://example.com\r\n";
  static const char *key_2_value = "GET http://example.com\r\nlater\r\n";
  static const char *header_1_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n";
  static const char *header_2_value =
    "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nLast-Modified: later\r\n\r\n";
  static const char *body_1_value = "Hello, world!";
  static const char *body_2_value = "Hello, world 2!";
  const string_desc_t key_1 = {._nbytes = strlen (key_1_value),._data =
      (char *) key_1_value
  };
  const string_desc_t key_2 = {._nbytes = strlen (key_2_value),._data =
      (char *) key_2_value
  };
  const string_desc_t header_1 = {._nbytes = strlen (header_1_value),._data =
      (char *) header_1_value
  };
  const string_desc_t header_2 = {._nbytes = strlen (header_2_value),._data =
      (char *) header_2_value
  };
  const string_desc_t body_1 = {._nbytes = strlen (body_1_value),._data =
      (char *) body_1_value
  };
  const string_desc_t body_2 = {._nbytes = strlen (body_2_value),._data =
      (char *) body_2_value
  };
  const struct timespec request_date_1 = {.tv_sec = 12345,.tv_nsec = 678910 };
  const struct timespec response_date_1 = {.tv_sec = 111213,.tv_nsec = 141516
  };
  const struct timespec request_date_2 = {.tv_sec = 22345,.tv_nsec = 678920 };
  const struct timespec response_date_2 = {.tv_sec = 222223,.tv_nsec = 242526
  };
  char filename[] = "/tmp/test-ao-cache-entry-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.entry";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = 0;
  size_t top;
  error = ao_file_prepare (file, file_magic);
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  size_t offset_1, offset_2;
  error =
    /* Notice the "false" here. */
    ao_cache_entry_push (file, &offset_1, &request_date_1, &response_date_1,
			 false, key_1, header_1, body_1);
  ck_assert_int_eq (error, 0);
  error =
    /* Notice the "false" here too. */
    ao_cache_entry_push (file, &offset_2, &request_date_2, &response_date_2,
			 false, key_2, header_2, body_2);
  ck_assert_int_eq (error, 0);
  size_t offsets[] = { offset_1, offset_2 };
  error =
    ao_cache_group_push (file, &top, sizeof (offsets) / sizeof (offsets[0]),
			 offsets);
  ck_assert_int_eq (error, 0);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  size_t invalidated_offset = 42;
  error = ao_cache_group_invalidate (file, top, &invalidated_offset);
  ck_assert_int_eq (error, 0);
  ck_assert_int_gt (invalidated_offset, top);
  size_t *final_offsets = NULL;
  size_t n_entries = 42;
  error =
    ao_cache_group_read (file, invalidated_offset, &n_entries,
			 &final_offsets);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (n_entries, 2);
  ck_assert_int_gt (final_offsets[0], offset_1);
  ck_assert_int_gt (final_offsets[1], offset_2);
  string_desc_t check_key = { 0 };
  string_desc_t check_header = { 0 };
  string_desc_t check_body = { 0 };
  struct timespec check_request_date, check_response_date;
  bool check_invalidated;
  error =
    ao_cache_entry_read (file, final_offsets[0], &check_request_date,
			 &check_response_date, &check_invalidated, true, true,
			 true, &check_key, &check_header, &check_body);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (check_request_date.tv_sec, 12345);
  ck_assert_int_eq (check_request_date.tv_nsec, 678910);
  ck_assert_int_eq (check_response_date.tv_sec, 111213);
  ck_assert_int_eq (check_response_date.tv_nsec, 141516);
  ck_assert (check_invalidated);
  ck_assert_int_eq (check_key._nbytes, strlen (key_1_value));
  ck_assert_int_eq (check_header._nbytes, strlen (header_1_value));
  ck_assert_int_eq (check_body._nbytes, strlen (body_1_value));
  ck_assert_int_eq (memcmp
		    (check_key._data, key_1_value, strlen (key_1_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_header._data, header_1_value,
		     strlen (header_1_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_body._data, body_1_value, strlen (body_1_value)),
		    0);
  FREE (check_key._data);
  FREE (check_header._data);
  FREE (check_body._data);
  error =
    ao_cache_entry_read (file, final_offsets[1], &check_request_date,
			 &check_response_date, &check_invalidated, true, true,
			 true, &check_key, &check_header, &check_body);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (check_request_date.tv_sec, 22345);
  ck_assert_int_eq (check_request_date.tv_nsec, 678920);
  ck_assert_int_eq (check_response_date.tv_sec, 222223);
  ck_assert_int_eq (check_response_date.tv_nsec, 242526);
  ck_assert (check_invalidated);
  ck_assert_int_eq (check_key._nbytes, strlen (key_2_value));
  ck_assert_int_eq (check_header._nbytes, strlen (header_2_value));
  ck_assert_int_eq (check_body._nbytes, strlen (body_2_value));
  ck_assert_int_eq (memcmp
		    (check_key._data, key_2_value, strlen (key_2_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_header._data, header_2_value,
		     strlen (header_2_value)), 0);
  ck_assert_int_eq (memcmp
		    (check_body._data, body_2_value, strlen (body_2_value)),
		    0);
  FREE (check_key._data);
  FREE (check_header._data);
  FREE (check_body._data);
  FREE (final_offsets);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_cache_key_no_host)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, -1);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_cache_key_invalid_request_line)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Hello\r\n\
Host: example.com\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, -1);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_cache_key_invalid_response_line)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Host: example.com\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
This line is not HTTP/1.1.\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, -1);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_simple_cache_key)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Host: example.com\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error = compute_cache_key ("https", request, response, 0, memory);
  ck_assert_int_eq (error, -2);
  error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, 0);
  ck_assert_str_eq (memory, "GET https://example.com/example\r\n");
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_simply_varied_cache_key)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Host: example.com\r\n\
Accept: text/plain\r\n\
Foo: bar\r\n\
Foo: other, thing\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
ETag: W/\"hello\"\r\n\
Vary: accept, foo\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error = compute_cache_key ("https", request, response, 0, memory);
  ck_assert_int_eq (error, -2);
  error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, 0);
  ck_assert_str_eq (memory, "\
GET https://example.com/example\r\n\
text/plain\r\n\
bar,other, thing\r\n");
  /* FIXME: "other, thing" is not touched so the space is
     kept. Should disfluid try and parse comma-separated lists? Also
     see test_multiply_varied_cache_key for another occurence of the
     same problem. */
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_multiply_varied_cache_key)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Host: example.com\r\n\
Accept: text/plain\r\n\
Foo: bar\r\n\
Foo: other, thing\r\n\
If-None-Match: W/\"hello\", \"world\"\r\n\
If-None-Match: W/\"hi :)\"\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
ETag: W/\"hello\"\r\n\
Vary: accept, foo\r\n\
Vary: accept\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error = compute_cache_key ("https", request, response, 0, memory);
  ck_assert_int_eq (error, -2);
  error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, 0);
  ck_assert_str_eq (memory, "\
GET https://example.com/example\r\n\
text/plain\r\n\
bar,other, thing\r\n\
text/plain\r\n");
  /* FIXME: see the test_simply_varied_cache_key test. */
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_partial_cache_key)
/* *INDENT-ON* */

{
  static const char *request = "\
GET /example HTTP/1.1\r\n\
Host: example.com\r\n\
Accept: text/plain\r\n\
\r\n";
  static const char *response = "\
HTTP/1.1 200 Hi\r\n\
Content-Type: text/plain\r\n\
Vary: a\r\n\
\r\n\
Hi :)";
  char memory[512];
  int error =
    compute_cache_key ("https", request, response, sizeof (memory), memory);
  ck_assert_int_eq (error, 0);
  ck_assert_str_eq (memory, "\
GET https://example.com/example\r\n\
\r\n");				/* Notice how the "a" header is not present in the request, so
				   the value is just empty. */
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_cache_hash)
/* *INDENT-ON* */

{
  static const char *method = "GET";
  static const char *uri = "https://example.com";
  static const char *password = "hello :)";
  char output[65];
  int error =
    hash_primary_cache_key (method, uri, password, strlen (password),
			    sizeof (output), output);
  ck_assert_int_eq (error, 0);
  ck_assert_str_eq (output, "\
3ee6f03368423b1e7da285a42f049a8e\
9af16f60337cc02fb1bfe5d7aeab6fc8");
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_aof_recover)
/* *INDENT-ON* */

{
  /* Here we have a partially updated file. The top was offset 5, but
     is in the process of being updated to 13. In the mean time, the
     first offset is garbage. */
  static const uint8_t partial_update[] = {
    /* Magic: */
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
    /* Status: */
    1, 42, 42, 42, 42, 42, 42, 42,
    /* Top before: */
    42, 42, 42, 42, 42, 42, 42, 42,
    /* Top after: */
    0, 0, 0, 0, 0, 0, 0, 13,
    /* Data: */
    72, 101, 108, 108, 111, 44, 32, 119, 111, 114, 108, 100, 33
  };
  char filename[] = "/tmp/test-partial-update-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  size_t n_to_write = sizeof (partial_update);
  const uint8_t *to_write = partial_update;
  while (n_to_write > 0)
    {
      ssize_t n_written = write (file, to_write, n_to_write);
      ck_assert_int_gt (n_written, 0);
      ck_assert_int_le (n_written, n_to_write);
      n_to_write -= n_written;
      to_write += n_written;
    }
  size_t top = 42;
  int error = ao_file_read_top (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 13);
  if (lseek (file, 0, SEEK_SET) == -1)
    {
      ck_assert (false);
    }
  static const uint8_t after_update_expected[] = {
    /* Magic: */
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
    /* Status: */
    0, 42, 42, 42, 42, 42, 42, 42,
    /* Top before: */
    0, 0, 0, 0, 0, 0, 0, 13,
    /* Top after: */
    0, 0, 0, 0, 0, 0, 0, 13,
    /* Data: */
    72, 101, 108, 108, 111, 44, 32, 119, 111, 114, 108, 100, 33
  };
  uint8_t actual_data[sizeof (after_update_expected)] = { 0 };
  size_t n_to_read = sizeof (after_update_expected);
  uint8_t *read_ptr = actual_data;
  while (n_to_read > 0)
    {
      ssize_t n_read = read (file, read_ptr, n_to_read);
      ck_assert_int_gt (n_read, 0);
      ck_assert_int_le (n_read, n_to_read);
      n_to_read -= n_read;
      read_ptr += n_read;
    }
  for (size_t i = 0; i < sizeof (after_update_expected); i++)
    {
      ck_assert_int_eq (actual_data[i], after_update_expected[i]);
    }
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_aof_can_read_locked_file)
/* *INDENT-ON* */

{
  /* Here we construct a file with "Hello", and lock it. We should
     still be able to call ao_file_read_top. */
  char filename[] = "/tmp/test-lock-read-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid test ao";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = ao_file_prepare (file, file_magic);
  ck_assert_int_eq (error, 0);
  size_t top;
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 0);
  static const char *hello_data = "Hello";
  const string_desc_t hello = {
    ._data = (char *) hello_data,
    ._nbytes = strlen (hello_data)
  };
  error = ao_file_push_data (file, hello, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 5);
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  static const char *world_data = ", world!";
  const string_desc_t world = {
    ._data = (char *) world_data,
    ._nbytes = strlen (world_data)
  };
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 5);
  error = ao_file_push_data (file, world, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 13);
  /* file is still locked. */
  error = ao_file_read_top (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 5);
  ao_file_abort_transaction (file);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

enum test_aof_trie_fold_step
{
  TEST_AOF_TRIE_FOLD_STEP_DOWN_A = 0,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_AA,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_AAA,
  /* Skip */
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AAA,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_AAB,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_AABNUL,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AABNUL,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AAB,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AA,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_A,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_B,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_BA,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_BAB,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_BABA,
  TEST_AOF_TRIE_FOLD_STEP_DOWN_BABANUL,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BABANUL,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BABA,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BAB,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BA,
  TEST_AOF_TRIE_FOLD_STEP_UP_FROM_B,
  TEST_AOF_TRIE_FOLD_DONE
};

static int
test_aof_trie_fold_down (void *context, char key, size_t offset,
			 bool *explore)
{
  enum test_aof_trie_fold_step *step = context;
  *explore = true;
  switch (*step)
    {
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_A:
      ck_assert_int_eq (key, 'a');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_AA:
      ck_assert_int_eq (key, 'a');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_AAA:
      ck_assert_int_eq (key, 'a');
      ck_assert_int_lt (offset, 8000);
      *explore = false;
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_AAB:
      ck_assert_int_eq (key, 'b');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_AABNUL:
      ck_assert_int_eq (key, '\0');
      ck_assert_int_eq (offset, 8003);
      *explore = false;
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_B:
      ck_assert_int_eq (key, 'b');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_BA:
      ck_assert_int_eq (key, 'a');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_BAB:
      ck_assert_int_eq (key, 'b');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_BABA:
      ck_assert_int_eq (key, 'a');
      ck_assert_int_lt (offset, 8000);
      break;
    case TEST_AOF_TRIE_FOLD_STEP_DOWN_BABANUL:
      ck_assert_int_eq (key, '\0');
      ck_assert_int_eq (offset, 8001);
      *explore = false;
      break;
    default:
      ck_assert (false);
    }
  *step += 1;
  return 0;
}

static int
test_aof_trie_fold_up (void *context)
{
  enum test_aof_trie_fold_step *step = context;
  switch (*step)
    {
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AAA:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AABNUL:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AAB:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_AA:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_A:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BABANUL:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BABA:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BAB:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_BA:
    case TEST_AOF_TRIE_FOLD_STEP_UP_FROM_B:
      break;
    default:
      ck_assert (false);
    }
  *step += 1;
  return 0;
}

/* *INDENT-OFF* */
START_TEST (test_aof_trie_fold)
/* *INDENT-ON* */

{
  /* The trie has: aaab (offset 8000), baba (8001), aaa (8002) and aab
     (8003). We want to skip "aaa*". We should find in order: aab and
     baba. */
  char filename[] = "/tmp/test-ao-trie-fold-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid trie ao";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = ao_file_prepare (file, file_magic);
  ck_assert_int_eq (error, 0);
  size_t top;
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 0);
  /* Push aaab$. */
  static const uint8_t aaab_key = '\0';
  static const size_t aaab_value = 8000;
  size_t aaab_offset;
  error = ao_trie_push (file, &aaab_offset, 1, &aaab_key, &aaab_value);
  ck_assert_int_eq (error, 0);
  /* Push aaa*. */
  static const uint8_t aaa_keys[] = { 'b', '\0' };
  const size_t aaa_values[] = { aaab_offset, 8002 };
  size_t aaa_offset;
  error = ao_trie_push (file, &aaa_offset, 2, aaa_keys, aaa_values);
  ck_assert_int_eq (error, 0);
  /* Push aab$. */
  static const uint8_t aab_key = '\0';
  static const size_t aab_value = 8003;
  size_t aab_offset;
  error = ao_trie_push (file, &aab_offset, 1, &aab_key, &aab_value);
  ck_assert_int_eq (error, 0);
  /* Push aa*. */
  static const uint8_t aa_keys[] = { 'b', 'a' };
  const size_t aa_values[] = { aab_offset, aaa_offset };
  size_t aa_offset;
  error = ao_trie_push (file, &aa_offset, 2, aa_keys, aa_values);
  ck_assert_int_eq (error, 0);
  /* Push a*. */
  static const uint8_t a_keys[] = { 'a' };
  const size_t a_values[] = { aa_offset };
  size_t a_offset;
  error = ao_trie_push (file, &a_offset, 1, a_keys, a_values);
  ck_assert_int_eq (error, 0);
  /* Push baba$. */
  static const uint8_t baba_keys[] = { '\0' };
  const size_t baba_values[] = { 8001 };
  size_t baba_offset;
  error = ao_trie_push (file, &baba_offset, 1, baba_keys, baba_values);
  ck_assert_int_eq (error, 0);
  /* Push bab*. */
  static const uint8_t bab_keys[] = { 'a' };
  const size_t bab_values[] = { baba_offset };
  size_t bab_offset;
  error = ao_trie_push (file, &bab_offset, 1, bab_keys, bab_values);
  ck_assert_int_eq (error, 0);
  /* Push ba*. */
  static const uint8_t ba_keys[] = { 'b' };
  const size_t ba_values[] = { bab_offset };
  size_t ba_offset;
  error = ao_trie_push (file, &ba_offset, 1, ba_keys, ba_values);
  ck_assert_int_eq (error, 0);
  /* Push b*. */
  static const uint8_t b_keys[] = { 'a' };
  const size_t b_values[] = { ba_offset };
  size_t b_offset;
  error = ao_trie_push (file, &b_offset, 1, b_keys, b_values);
  ck_assert_int_eq (error, 0);
  /* Push the root. */
  static const uint8_t root_keys[] = { 'b', 'a' };
  const size_t root_values[] = { b_offset, a_offset };
  size_t root_offset;
  error = ao_trie_push (file, &root_offset, 2, root_keys, root_values);
  ck_assert_int_eq (error, 0);
  /* Commit */
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  /* Check */
  enum test_aof_trie_fold_step step = 0;
  error =
    ao_trie_fold (file, root_offset, test_aof_trie_fold_down,
		  test_aof_trie_fold_up, &step);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (step, TEST_AOF_TRIE_FOLD_DONE);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_aof_cache_group)
/* *INDENT-ON* */

{
  /* The group has 2 entries: 8001 and 8002. */
  char filename[] = "/tmp/test-ao-cache-group-XXXXXX";
  int file = mkstemp (filename);
  ck_assert_int_ge (file, 0);
  static const char *magic_data = "disfluid c.group";
  const string_desc_t file_magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  int error = ao_file_prepare (file, file_magic);
  ck_assert_int_eq (error, 0);
  size_t top;
  error = ao_file_lock_for_writing (file, &top);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (top, 0);
  static const size_t expected_offsets[] = { 8001, 8002 };
  size_t final_offset = 42;
  error =
    ao_cache_group_push (file, &final_offset,
			 sizeof (expected_offsets) /
			 sizeof (expected_offsets[0]), expected_offsets);
  ck_assert_int_eq (error, 0);
  /* The array is 16 bytes, and the header too. */
  ck_assert_int_eq (final_offset, 32);
  /* Commit */
  error = ao_file_commit_transaction (file);
  ck_assert_int_eq (error, 0);
  /* Check */
  size_t actual_n_offsets = 42;
  size_t *actual_offsets = NULL;
  error =
    ao_cache_group_read (file, final_offset, &actual_n_offsets,
			 &actual_offsets);
  ck_assert_int_eq (error, 0);
  ck_assert_int_eq (actual_n_offsets, 2);
  ck_assert_ptr_nonnull (actual_offsets);
  ck_assert_int_eq (actual_offsets[0], 8001);
  ck_assert_int_eq (actual_offsets[1], 8002);
  FREE (actual_offsets);
  remove (filename);
  close (file);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_activity_context_1)
/* *INDENT-ON* */

{
  static const char *activity =
    "{"
    "\"@context\": \"https://www.w3.org/ns/activitystreams\","
    "\"summary\": \"A note\","
    "\"type\": \"Note\"," "\"content\": \"My dog has fleas.\"" "}";
  json_error_t error;
  json_t *object = json_loads (activity, 0, &error);
  ck_assert_ptr_nonnull (object);
  string_desc_t prefix;
  int ctx_error = activity_object_context_prefix (object, &prefix);
  ck_assert_int_eq (ctx_error, 0);
  ck_assert_int_eq (prefix._nbytes, 0);
  ck_assert_ptr_null (prefix._data);
  json_decref (object);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_activity_context_2)
/* *INDENT-ON* */

{
  static const char *activity =
    "{"
    "\"@context\": {"
    "\"@vocab\": \"https://www.w3.org/ns/activitystreams#\","
    "\"ext\": \"https://canine-extension.example/terms/\","
    "\"@language\": \"en\""
    "},"
    "\"summary\": \"A note\","
    "\"type\": \"Note\","
    "\"content\": \"My dog has fleas.\","
    "\"ext:nose\": 0," "\"ext:smell\": \"terrible\"" "}";
  json_error_t error;
  json_t *object = json_loads (activity, 0, &error);
  ck_assert_ptr_nonnull (object);
  string_desc_t prefix;
  int ctx_error = activity_object_context_prefix (object, &prefix);
  ck_assert_int_eq (ctx_error, 0);
  ck_assert_int_eq (prefix._nbytes, 0);
  ck_assert_ptr_null (prefix._data);
  json_decref (object);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_activity_context_2bis)
/* *INDENT-ON* */

{
  static const char *activity =
    "{"
    "\"@context\": {"
    "\"@vocab\": \"https://example.com/\","
    "\"as\": \"https://www.w3.org/ns/activitystreams#\","
    "\"ext\": \"https://canine-extension.example/terms/\","
    "\"@language\": \"en\""
    "},"
    "\"as:summary\": \"A note\","
    "\"as:type\": \"Note\","
    "\"as:content\": \"My dog has fleas.\","
    "\"ext:nose\": 0," "\"ext:smell\": \"terrible\"" "}";
  json_error_t error;
  json_t *object = json_loads (activity, 0, &error);
  ck_assert_ptr_nonnull (object);
  string_desc_t prefix;
  int ctx_error = activity_object_context_prefix (object, &prefix);
  ck_assert_int_eq (ctx_error, 0);
  ck_assert_int_eq (prefix._nbytes, 3);
  ck_assert_int_eq (memcmp (prefix._data, "as:", 3), 0);
  FREE (prefix._data);
  json_decref (object);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_activity_context_3)
/* *INDENT-ON* */

{
  static const char *activity =
    "{"
    "\"@context\": ["
    "\"https://www.w3.org/ns/activitystreams\","
    "{"
    "\"css\": \"http://www.w3.org/ns/oa#styledBy\""
    "}"
    "],"
    "\"summary\": \"A note\","
    "\"type\": \"Note\","
    "\"content\": \"My dog has fleas.\","
    "\"css\": \"http://www.csszengarden.com/217/217.css?v=8may2013\"" "}";
  json_error_t error;
  json_t *object = json_loads (activity, 0, &error);
  ck_assert_ptr_nonnull (object);
  string_desc_t prefix;
  int ctx_error = activity_object_context_prefix (object, &prefix);
  ck_assert_int_eq (ctx_error, 0);
  ck_assert_int_eq (prefix._nbytes, 0);
  ck_assert_ptr_null (prefix._data);
  json_decref (object);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

/* *INDENT-OFF* */
START_TEST (test_activity_context_none)
/* *INDENT-ON* */

{
  static const char *activity =
    "{"
    "\"http://schema.org/name\": \"Manu Sporny\","
    "\"http://schema.org/url\": {"
    "\"@id\": \"http://manu.sporny.org/\""
    "},"
    "\"http://schema.org/image\": {"
    "\"@id\": \"http://manu.sporny.org/images/manu.png\"" "}" "}";
  json_error_t error;
  json_t *object = json_loads (activity, 0, &error);
  ck_assert_ptr_nonnull (object);
  string_desc_t prefix;
  int ctx_error = activity_object_context_prefix (object, &prefix);
  ck_assert_int_lt (ctx_error, 0);
  ck_assert_int_eq (prefix._nbytes, 0);
  ck_assert_ptr_null (prefix._data);
  json_decref (object);
}
/* *INDENT-OFF* */
END_TEST
/* *INDENT-ON* */

static inline char *
tests_read_whole_file (int file)
{
  off_t size = lseek (file, 0, SEEK_END);
  lseek (file, 0, SEEK_SET);
  char *ret = malloc (size + 1);
  if (ret == NULL)
    {
      return NULL;
    }
  size_t n_total = 0;
  while ((off_t) n_total < size)
    {
      ssize_t next = read (file, ret + n_total, size - n_total);
      if (next <= 0)
	{
	  free (ret);
	  return NULL;
	}
      n_total += next;
    }
  ret[size] = '\0';
  return ret;
}

static inline char *
run_tests (size_t *n_tests, size_t *n_errors)
{
  ensure_init ();
  Suite *suite = suite_create (_("disfluid unit tests"));
  TCase *general = tcase_create (_("disfluid general tests"));
  tcase_add_test (general, test_check_version);
  suite_add_tcase (suite, general);
  TCase *cache_entry = tcase_create (_("disfluid cache entry files"));
  tcase_add_test (cache_entry, test_read_normal);
  tcase_add_test (cache_entry, test_invalidate_cache_entry_noop);
  tcase_add_test (cache_entry, test_invalidate_cache_entry);
  suite_add_tcase (suite, cache_entry);
  TCase *cache_group = tcase_create (_("disfluid cache group files"));
  tcase_add_test (cache_group, test_aof_cache_group);
  tcase_add_test (cache_group, test_invalidate_cache_group_noop);
  tcase_add_test (cache_group, test_invalidate_cache_group_partial);
  tcase_add_test (cache_group, test_invalidate_cache_group_all);
  suite_add_tcase (suite, cache_group);
  TCase *cache_key = tcase_create (_("disfluid cache key"));
  tcase_add_test (cache_key, test_cache_key_no_host);
  tcase_add_test (cache_key, test_cache_key_invalid_request_line);
  tcase_add_test (cache_key, test_cache_key_invalid_response_line);
  tcase_add_test (cache_key, test_simple_cache_key);
  tcase_add_test (cache_key, test_simply_varied_cache_key);
  tcase_add_test (cache_key, test_multiply_varied_cache_key);
  tcase_add_test (cache_key, test_partial_cache_key);
  suite_add_tcase (suite, cache_key);
  TCase *cache_hash = tcase_create (_("disfluid cache hash key"));
  tcase_add_test (cache_hash, test_cache_hash);
  suite_add_tcase (suite, cache_hash);
  TCase *aof = tcase_create (_("append-only file"));
  tcase_add_test (aof, test_aof_recover);
  tcase_add_test (aof, test_aof_can_read_locked_file);
  tcase_add_test (aof, test_aof_trie_fold);
  suite_add_tcase (suite, aof);
  TCase *activity = tcase_create (_("activity"));
  tcase_add_test (activity, test_activity_context_1);
  tcase_add_test (activity, test_activity_context_2);
  tcase_add_test (activity, test_activity_context_2bis);
  tcase_add_test (activity, test_activity_context_3);
  tcase_add_test (activity, test_activity_context_none);
  suite_add_tcase (suite, activity);
  SRunner *runner = srunner_create (suite);
  char log_file_name[] = "/tmp/disfluid-unit-tests-XXXXXX";
  int log_file = mkstemp (log_file_name);
  srunner_set_log (runner, log_file_name);
  srunner_run_all (runner, CK_NORMAL);
  char *result = tests_read_whole_file (log_file);
  if (n_tests)
    {
      *n_tests = srunner_ntests_run (runner);
    }
  if (n_errors)
    {
      *n_errors = srunner_ntests_failed (runner);
    }
  srunner_free (runner);
  close (log_file);
  remove (log_file_name);
  return result;
}

static inline int
test_append_init (int *fd, char **filename)
{
  ensure_init ();
  int error = 0;
  *fd = -1;
  *filename = NULL;
  static const char model[] = "/tmp/disfluid-test-append-XXXXXX";
  if (ALLOC_N (*filename, strlen (model) + 1) < 0)
    {
      error = -2;
      goto cleanup;
    }
  strcpy (*filename, model);
  *fd = mkstemp (*filename);
  if (*fd == -1)
    {
      error = -1;
      FREE (*filename);
      goto cleanup;
    }
  static const char *magic_data = "disfluid appendt";
  const string_desc_t magic = {
    ._data = (char *) magic_data,
    ._nbytes = strlen (magic_data)
  };
  if (ao_file_prepare (*fd, magic) < 0)
    {
      error = -1;
      goto cleanup;
    }
cleanup:
  if (error != 0)
    {
      FREE (*filename);
      if (*fd >= 0)
	{
	  close (*fd);
	  *fd = -1;
	}
    }
  return error;
}

static inline int
test_append_hello (const char *filename, size_t *n_before, size_t *n_after)
{
  ensure_init ();
  if (test_append_count (filename, n_before) < 0)
    {
      return -1;
    }
  int file = open (filename, O_RDWR);
  int error = 0;
  if (file == -1)
    {
      return -1;
    }
  size_t top;
  if (ao_file_lock_for_writing (file, &top) < 0)
    {
      error = -1;
      goto cleanup;
    }
  static const char *hello_data = "Hello!";
  const string_desc_t hello = {
    ._nbytes = strlen (hello_data),
    ._data = (char *) hello_data
  };
  if (ao_file_push_data (file, hello, &top) < 0)
    {
      error = -1;
      goto cleanup;
    }
  if (ao_file_commit_transaction (file) < 0)
    {
      error = -1;
      goto cleanup;
    }
cleanup:
  close (file);
  if (error == 0)
    {
      if (test_append_count (filename, n_after) < 0)
	{
	  return -1;
	}
      if (n_before >= n_after)
	{
	  return -1;
	}
    }
  return error;
}

static inline int
test_append_count (const char *filename, size_t *n)
{
  ensure_init ();
  *n = 0;
  size_t top;
  int error = 0;
  int file = open (filename, O_RDWR);
  if (file == -1)
    {
      return -1;
    }
  if (ao_file_read_top (file, &top) < 0)
    {
      error = -1;
      goto cleanup;
    }
  if (top % strlen ("Hello!") != 0)
    {
      error = -2;
      goto cleanup;
    }
  while (top != 0)
    {
      char data[6];
      assert (sizeof (data) == strlen ("Hello!"));
      string_desc_t next = {._nbytes = sizeof (data),._data = data };
      if (ao_file_read (file, top, next) < 0)
	{
	  error = -2;
	  goto cleanup;
	}
      if (memcmp (data, "Hello!", sizeof (data)) != 0)
	{
	  error = -2;
	  goto cleanup;
	}
      top -= sizeof (data);
      *n += 1;
    }
cleanup:
  close (file);
  return error;
}

#endif /* DISFLUID_TESTS_INCLUDED */
