#ifndef DISFLUID_DISFLUID_CACHE_ENTRY_HASH_INCLUDED
# define DISFLUID_DISFLUID_CACHE_ENTRY_HASH_INCLUDED

MAYBE_UNUSED static int
hash_primary_cache_key (const char *restrict method,
			const char *restrict uri,
			const char *restrict password,
			size_t password_length,
			size_t max_hash, char *restrict hash);

# include <gnutls/gnutls.h>
# include <gnutls/crypto.h>
# include <assert.h>

# include "disfluid-init.h"

static int
hash_primary_cache_key (const char *restrict method,
			const char *restrict uri,
			const char *restrict password,
			size_t password_length,
			size_t max_hash, char *restrict hash)
{
  ensure_init ();
  gnutls_hmac_hd_t context;
  gnutls_mac_algorithm_t algo = GNUTLS_MAC_SHA256;
  int error = gnutls_hmac_init (&context, algo, password, password_length);
  if (error != 0)
    {
      return -1;
    }
  uint8_t digest[32];
  if (gnutls_hmac_get_len (algo) != sizeof (digest))
    {
      /* Unreachable. */
      assert (0);
    }
  error = gnutls_hmac (context, method, strlen (method));
  if (error == 0)
    {
      error = gnutls_hmac (context, " ", strlen (" "));
    }
  if (error == 0)
    {
      error = gnutls_hmac (context, uri, strlen (uri));
    }
  gnutls_hmac_deinit (context, digest);
  if (error != 0)
    {
      return -1;
    }
  gnutls_datum_t data = {
    .data = digest,
    .size = sizeof (digest)
  };
  if (gnutls_hex_encode (&data, hash, &max_hash) != 0)
    {
      return -2;
    }
  return 0;
}

#endif /* DISFLUID_DISFLUID_CACHE_ENTRY_HASH_INCLUDED */
