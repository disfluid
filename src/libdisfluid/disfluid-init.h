#ifndef DISFLUID_INIT_INCLUDED
# define DISFLUID_INIT_INCLUDED

static inline void ensure_init (void);

# include "relocatable.h"
# include "safe-alloc.h"

static inline void
ensure_init (void)
{
  static volatile int is_initialized = 0;
  if (!is_initialized)
    {
      char *relocated = NULL;
      const char *localedir = relocate2 (LOCALEDIR, &relocated);
      bindtextdomain (PACKAGE, localedir);
      FREE (relocated);
      is_initialized = 1;
    }
}

#endif /* DISFLUID_INIT_INCLUDED */
