#ifndef DISFLUID_UI_INCLUDED
# define DISFLUID_UI_INCLUDED

static inline const char *greet_world (void);
static inline const char *metaphor_name (void);

# include "disfluid-init.h"

static inline const char *
greet_world (void)
{
  ensure_init ();
  return _("Hello, world!");
}

static inline const char *
metaphor_name (void)
{
  ensure_init ();
  return _("Experiences");
}

#endif /* DISFLUID_UI_INCLUDED */
