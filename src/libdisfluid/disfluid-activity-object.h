#ifndef DISFLUID_ACTIVITY_OBJECT_INCLUDED
# define DISFLUID_ACTIVITY_OBJECT_INCLUDED

# include <config.h>
# include "string-desc.h"
# include <jansson.h>

MAYBE_UNUSED static int
activity_object_context_prefix (json_t * object,
				string_desc_t * context_prefix);

# include "disfluid-append-only-file.h"
# include "safe-alloc.h"

static bool
activity_object_is_as (json_t * value, bool with_hash)
{
  static const char *as = "https://www.w3.org/ns/activitystreams#";
  const size_t as_len_h = strlen (as);
  const size_t as_len_s = as_len_h - 1;
  size_t as_len = as_len_s;
  if (with_hash)
    {
      as_len = as_len_h;
    }
  return (json_is_string (value)
	  && json_string_length (value) == as_len
	  && strncmp (json_string_value (value), as, as_len) == 0);
}

static int
activity_object_context_prefix (json_t * object,
				string_desc_t * context_prefix)
{
  context_prefix->_nbytes = 0;
  context_prefix->_data = NULL;
  if (!json_is_object (object))
    {
      return -1;
    }
  json_t *context = json_object_get (object, "@context");
  if (activity_object_is_as (context, false))
    {
      /* This is the only context. */
      context_prefix->_nbytes = 0;
      context_prefix->_data = NULL;
      return 0;
    }
  else if (json_is_object (context))
    {
      json_t *vocab = json_object_get (context, "@vocab");
      if (activity_object_is_as (vocab, true))
	{
	  /* Also not prefixed. */
	  context_prefix->_nbytes = 0;
	  context_prefix->_data = NULL;
	  return 0;
	}
      /* Maybe it is a value of the context object. */
      const char *key;
      json_t *value;
      context_prefix->_nbytes = 0;
      context_prefix->_data = NULL;
      json_object_foreach (context, key, value)
      {
	if (key[0] != '@' && activity_object_is_as (value, true))
	  {
	    FREE (context_prefix->_data);
	    context_prefix->_nbytes = strlen (key) + 1;	/* for the final : */
	    if (ALLOC_N (context_prefix->_data, context_prefix->_nbytes) < 0)
	      {
		return -2;
	      }
	    memcpy (context_prefix->_data, key, context_prefix->_nbytes - 1);
	    context_prefix->_data[context_prefix->_nbytes - 1] = ':';
	  }
      }
      if (context_prefix->_data != NULL)
	{
	  return 0;
	}
    }
  else if (json_is_array (context))
    {
      size_t n = json_array_size (context);
      for (size_t i = 0; i < n; i++)
	{
	  json_t *value = json_array_get (context, i);
	  if (activity_object_is_as (value, false))
	    {
	      context_prefix->_nbytes = 0;
	      context_prefix->_data = NULL;
	      return 0;
	    }
	}
    }
  return -1;
}

#endif /* not DISFLUID_ACTIVITY_OBJECT_INCLUDED */
