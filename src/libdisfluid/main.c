#include <config.h>

#define STREQ(a, b) (strcmp ((a), (b)) == 0)
#define STRNEQ(a, b) (! (STREQ (a, b)))

#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "gettext.h"
#include "relocatable.h"
#include "attribute.h"
#include <disfluid.h>

#define _(String) dgettext (PACKAGE, (String))
#define N_(String) (String)

#include "disfluid-activity-object.h"
#include "disfluid-authors.h"
#include "disfluid-cache-entry.h"
#include "disfluid-cache-entry-key.h"
#include "disfluid-cache-entry-hash.h"
#include "disfluid-tests.h"
#include "disfluid-ui.h"
#include "disfluid-version.h"

const char *
disfluid_version (void)
{
  return version ();
}

int
disfluid_is_nightly (void)
{
  return is_nightly ();
}

const char *
disfluid_website (void)
{
  return website ();
}

const char *
disfluid_whats_new (void)
{
  return whats_new ();
}

const char *
disfluid_major_version (void)
{
  return major_version ();
}

char *
disfluid_run_tests (size_t *n_tests, size_t *n_errors)
{
  return run_tests (n_tests, n_errors);
}

int
disfluid_test_append_init (int *fd, char **filename)
{
  return test_append_init (fd, filename);
}

int
disfluid_test_append_hello (const char *filename, size_t *n_before,
			    size_t *n_after)
{
  return test_append_hello (filename, n_before, n_after);
}

int
disfluid_test_append_count (const char *filename, size_t *n)
{
  return test_append_count (filename, n);
}

size_t
disfluid_count_authors (void)
{
  return count_authors ();
}

const char *
disfluid_author_name (size_t i)
{
  return author_name (i);
}

const char *
disfluid_author_email (size_t i)
{
  return author_email (i);
}

const char *
disfluid_author_uri (size_t i)
{
  return author_uri (i);
}

int
disfluid_author_is_developer (size_t i)
{
  return author_is_developer (i);
}

int
disfluid_author_is_designer (size_t i)
{
  return author_is_designer (i);
}

int
disfluid_author_is_artist (size_t i)
{
  return author_is_artist (i);
}

int
disfluid_author_is_documenter (size_t i)
{
  return author_is_documenter (i);
}

const char *
disfluid_translation_credits (void)
{
  return translation_credits ();
}

char *
disfluid_copyright_statement (void)
{
  return copyright_statement ();
}

int
disfluid_compute_cache_key (const char *request_scheme,
			    const char *request_header,
			    const char *response_header,
			    size_t max_key, char *key)
{
  return compute_cache_key (request_scheme, request_header, response_header,
			    max_key, key);
}

int
disfluid_hash_primary_cache_key (const char *method,
				 const char *uri,
				 const char *password,
				 size_t password_length,
				 size_t max_hash, char *hash)
{
  return hash_primary_cache_key (method, uri, password, password_length,
				 max_hash, hash);
}

const char *
disfluid_greet_world (void)
{
  return greet_world ();
}

const char *
disfluid_metaphor_name (void)
{
  return metaphor_name ();
}
