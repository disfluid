#ifndef DISFLUID_TRIE_INCLUDED
# define DISFLUID_TRIE_INCLUDED

# include <config.h>
# include "string-desc.h"

MAYBE_UNUSED static int
ao_trie_read (int fd, size_t offset, size_t *n_keys, uint8_t * keys,
	      size_t *offsets);

MAYBE_UNUSED static int
ao_trie_push (int fd, size_t *offset, size_t n_keys, const uint8_t * keys,
	      const size_t *offsets);

MAYBE_UNUSED static int
ao_trie_fold (int fd, size_t offset,
	      int (*down) (void *context, char key, size_t offset,
			   bool *explore),
	      int (*up) (void *context), void *context);

# include "disfluid-append-only-file.h"
# include "safe-alloc.h"

static int
ao_trie_read (int fd,
	      size_t offset, size_t *n_keys, uint8_t * keys, size_t *offsets)
{
  uint8_t n_keys_data[8] = { 0 };
  string_desc_t n_keys_desc = {
    ._nbytes = sizeof (n_keys_data),
    ._data = n_keys_data
  };
  int error = 0;
  if (ao_file_read (fd, offset, n_keys_desc) < 0)
    {
      error = -1;
      goto cleanup;
    }
  *n_keys = 0;
  for (size_t i = 0; i < sizeof (n_keys_data); i++)
    {
      *n_keys *= 256;
      *n_keys += n_keys_data[i];
    }
  offset -= sizeof (n_keys_data);
  for (size_t i = 0; i < *n_keys; i++)
    {
      uint8_t entry_data[9] = { 0 };
      string_desc_t entry_desc = {
	._nbytes = sizeof (entry_data),
	._data = entry_data
      };
      if (ao_file_read (fd, offset, entry_desc) < 0)
	{
	  error = -1;
	  goto cleanup;
	}
      keys[i] = entry_data[0];
      offsets[i] = 0;
      for (size_t j = 1; j < sizeof (entry_data); j++)
	{
	  offsets[i] *= 256;
	  offsets[i] += entry_data[j];
	}
      offset -= sizeof (entry_data);
    }
cleanup:
  return error;
}

struct entry
{
  bool used;
  uint8_t key;
  size_t offset;
};

static int
ao_trie_push (int fd,
	      size_t *offset,
	      size_t n_keys, const uint8_t * keys, const size_t *offsets)
{
  struct entry entries[256];
  for (size_t i = 0; i < sizeof (entries) / sizeof (entries[0]); i++)
    {
      entries[i].used = false;
    }
  for (size_t i = 0; i < n_keys; i++)
    {
      if (entries[keys[i]].used)
	{
	  return -1;
	}
      entries[keys[i]].used = true;
      entries[keys[i]].key = keys[i];
      entries[keys[i]].offset = offsets[i];
    }
  for (size_t i = 256; i-- > 0;)
    {
      if (entries[i].used)
	{
	  uint8_t entry_data[9] = { entries[i].key, 0 };
	  size_t branch_offset = entries[i].offset;
	  for (size_t j = sizeof (entry_data); j-- > 1;)
	    {
	      entry_data[j] = branch_offset % 256;
	      branch_offset /= 256;
	    }
	  string_desc_t entry_desc = {
	    ._nbytes = sizeof (entry_data),
	    ._data = entry_data
	  };
	  if (ao_file_push_data (fd, entry_desc, offset) < 0)
	    {
	      return -1;
	    }
	}
    }
  size_t n_branches = n_keys;
  uint8_t count_data[8] = { 0 };
  for (size_t j = sizeof (count_data); j-- > 0;)
    {
      count_data[j] = n_branches % 256;
      n_branches /= 256;
    }
  string_desc_t count_desc = {
    ._nbytes = sizeof (count_data),
    ._data = count_data
  };
  if (ao_file_push_data (fd, count_desc, offset) < 0)
    {
      return -1;
    }
  return 0;
}

struct ao_trie_path
{
  struct ao_trie_path *parent;
  size_t n_keys;
  uint8_t keys[256];
  size_t suboffsets[256];
  size_t n_explored;
};

static int ao_trie_path_skip (struct ao_trie_path **path);

static int ao_trie_path_down (struct ao_trie_path **path, int fd);

static int ao_trie_path_up (struct ao_trie_path **path);

static int
ao_trie_fold (int fd, size_t offset,
	      int (*down) (void *context, char key, size_t offset,
			   bool *explore),
	      int (*up) (void *context), void *context)
{
  struct ao_trie_path root = {.parent = NULL,.n_keys = 0,.n_explored = 0 };
  if (ao_trie_read (fd, offset, &(root.n_keys), root.keys, root.suboffsets)
      < 0)
    {
      return -1;
    }
  struct ao_trie_path *path = &root;
  int error = 0;
  while (path->parent != NULL || path->n_explored < path->n_keys)
    {
      if (path->n_explored == path->n_keys)
	{
	  if (up (context) != 0)
	    {
	      error = -1;
	      goto cleanup;
	    }
	  if (ao_trie_path_up (&path) < 0)
	    {
	      error = -1;
	      goto cleanup;
	    }
	}
      else
	{
	  bool explore = false;
	  if (down
	      (context, path->keys[path->n_explored],
	       path->suboffsets[path->n_explored], &explore) != 0)
	    {
	      error = -1;
	      goto cleanup;
	    }
	  if (explore)
	    {
	      if (ao_trie_path_down (&path, fd) < 0)
		{
		  error = -2;
		  goto cleanup;
		}
	    }
	  else
	    {
	      if (up (context) != 0)
		{
		  error = -1;
		  goto cleanup;
		}
	      if (ao_trie_path_skip (&path) < 0)
		{
		  error = -2;
		  goto cleanup;
		}
	    }
	}
    }
cleanup:
  return error;
}

static int
ao_trie_path_skip (struct ao_trie_path **path)
{
  struct ao_trie_path *old_path = *path;
  old_path->n_explored++;
  return 0;
}

static int
ao_trie_path_down (struct ao_trie_path **path, int fd)
{
  struct ao_trie_path *old_path = *path;
  struct ao_trie_path *new_path;
  if (ALLOC (new_path) < 0)
    {
      return -2;
    }
  new_path->parent = old_path;
  new_path->n_keys = 0;
  new_path->n_explored = 0;
  if (ao_trie_read
      (fd, old_path->suboffsets[old_path->n_explored], &(new_path->n_keys),
       new_path->keys, new_path->suboffsets) < 0)
    {
      FREE (new_path);
      return -1;
    }
  *path = new_path;
  return 0;
}

static int
ao_trie_path_up (struct ao_trie_path **path)
{
  struct ao_trie_path *current = *path;
  *path = current->parent;
  (*path)->n_explored++;
  FREE (current);
  return 0;
}

#endif /* not DISFLUID_TRIE_INCLUDED */
