#ifndef DISFLUID_VERSION_INCLUDED
# define DISFLUID_VERSION_INCLUDED

# include "disfluid-init.h"

static inline const char *version (void);

static inline bool is_nightly (void);

static inline const char *website (void);

static inline const char *whats_new (void);

static inline const char *major_version (void);

static inline const char *
version (void)
{
  static const char *package_version = VERSION;
  return package_version;
}

static inline const char *
website (void)
{
  return "https://labo.planete-kraus.eu/disfluid.git";
}

static inline const char *
whats_new (void)
{
  ensure_init ();
  /* Updated for NEWS hash f0fb7934a30c4e89f0468433bd9317c7 */
  return _("<p>The Experiences app has been started.</p>");
}

static inline const char *
major_version (void)
{
  ensure_init ();
  static const char *major_version = LATEST_MAJOR_VERSION;
  return major_version;
}

static inline bool
is_nightly (void)
{
  return DISFLUID_IS_NIGHTLY;
}

#endif /* DISFLUID_VERSION_INCLUDED */
