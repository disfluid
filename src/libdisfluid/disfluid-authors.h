#ifndef DISFLUID_AUTHORS_INCLUDED
# define DISFLUID_AUTHORS_INCLUDED

# include "safe-alloc.h"

static inline size_t count_authors (void);

static inline const char *author_name (size_t i);

static inline const char *author_email (size_t i);

static inline const char *author_uri (size_t i);

static inline bool author_is_developer (size_t i);

static inline bool author_is_artist (size_t i);

static inline bool author_is_documenter (size_t i);

static inline const char *translation_credits (void);

static inline char *copyright_statement (void);

# include "disfluid-init.h"

struct disfluid_author
{
  const char *name;
  const char *email;
  const char *uri;
  bool is_developer;
  bool is_designer;
  bool is_artist;
  bool is_documenter;
};

static struct disfluid_author disfluid_authors[] = {
  {
   .name = N_("Vivien Kraus"),
   .email = "vivien@planete-kraus.eu",
   .uri = NULL,
   .is_developer = true,
   .is_designer = false,
   .is_artist = false,
   .is_documenter = false},
  /* Insert your info. Only the author name is required. Everything is
     exported through the public API. */
};

/* These git authors did not wish for their name to be listed by the
   API: */

/* [insert your name] */

static inline size_t
count_authors (void)
{
  return (sizeof (disfluid_authors) / sizeof (disfluid_authors[0]));
}

static inline const char *
author_name (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return _(disfluid_authors[i].name);
    }
  return NULL;
}

static inline const char *
author_email (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].email;
    }
  return NULL;
}

static inline const char *
author_uri (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].uri;
    }
  return NULL;
}

static inline bool
author_is_developer (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].is_developer;
    }
  return false;
}

static inline bool
author_is_designer (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].is_designer;
    }
  return false;
}

static inline bool
author_is_artist (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].is_artist;
    }
  return false;
}

static inline bool
author_is_documenter (size_t i)
{
  if (i < sizeof (disfluid_authors) / sizeof (disfluid_authors[0]))
    {
      return disfluid_authors[i].is_documenter;
    }
  return false;
}

static inline const char *
translation_credits (void)
{
  ensure_init ();
  return _("translator-credits");
}

static inline char *
copyright_statement (void)
{
  ensure_init ();
  int error = 0;
  char *ret = NULL;
  char *purpose;
  error = asprintf (&purpose, _("disfluid interoperable web stack"));
  if (error < 0)
    {
      goto cleanup;
    }
  char *copyright_line;
  error =
    asprintf (&copyright_line,
	      _("Copyright © %s  the respective authors"), "2023");
  if (error < 0)
    {
      goto cleanup_purpose;
    }
  char *disclaimer;
  error = asprintf (&disclaimer,
		    _("This program is free software: "
		      "you can redistribute it and/or modify it "
		      "under the terms of the GNU Affero General Public License "
		      "as published by the Free Software Foundation, "
		      "either version 3 of the License, "
		      "or (at your option) any later version.\n"
		      "This program is distributed in the hope that it will be useful, "
		      "but WITHOUT ANY WARRANTY; "
		      "without even the implied warranty of MERCHANTABILITY "
		      "or FITNESS FOR A PARTICULAR PURPOSE.  "
		      "See the GNU Affero General Public License for more details.\n"
		      "You should have received a copy "
		      "of the GNU Affero General Public License "
		      "along with this program.  "
		      "If not, see <https://www.gnu.org/licenses/>.\n"));
  if (error < 0)
    {
      goto cleanup_copyright_line;
    }
  error = asprintf (&ret, "%s\n%s\n%s", purpose, copyright_line, disclaimer);
  if (error < 0)
    {
      FREE (ret);
      ret = NULL;
      goto cleanup_disclaimer;
    }
cleanup_disclaimer:
  FREE (disclaimer);
cleanup_copyright_line:
  FREE (copyright_line);
cleanup_purpose:
  FREE (purpose);
cleanup:
  return ret;
}

#endif /* DISFLUID_AUTHORS_INCLUDED */
