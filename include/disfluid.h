#ifndef H_DISFLUID_INCLUDED
# define H_DISFLUID_INCLUDED

# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# if defined _WIN32 && !defined __CYGWIN__
#  define DISFLUID_DLL_MADNESS 1
# else
#  define DISFLUID_DLL_MADNESS 0
# endif

# if BUILDING_LIBDISFLUID && HAVE_VISIBILITY
#  define DISFLUID_DLL_EXPORTED __attribute__((__visibility__("default")))
# elif BUILDING_LIBDISFLUID && DISFLUID_DLL_MADNESS
#  define DISFLUID_DLL_EXPORTED __declspec(dllexport)
# elif DISFLUID_DLL_MADNESS
#  define DISFLUID_DLL_EXPORTED __declspec(dllimport)
# else
#  define DISFLUID_DLL_EXPORTED
# endif

# define DISFLUID_API \
  DISFLUID_DLL_EXPORTED

# ifdef ATTRIBUTE_DEALLOC
#  define DISFLUID_DEALLOC_WITH ATTRIBUTE_DEALLOC
# else
#  define DISFLUID_DEALLOC_WITH(f, i)
# endif

# ifdef ATTRIBUTE_DEALLOC_FREE
#  define DISFLUID_DEALLOC_WITH_FREE ATTRIBUTE_DEALLOC_FREE
# else
#  define DISFLUID_DEALLOC_WITH_FREE
# endif

# ifdef ATTRIBUTE_CONST
#  define DISFLUID_CONST ATTRIBUTE_CONST
# else
#  define DISFLUID_CONST
# endif

# ifdef ATTRIBUTE_PURE
#  define DISFLUID_PURE ATTRIBUTE_PURE
# else
#  define DISFLUID_PURE
# endif

# ifdef NODISCARD
#  define DISFLUID_NODISCARD NODISCARD
# else
#  define DISFLUID_NODISCARD
# endif

# ifdef ATTRIBUTE_FD_ARG
#  define DISFLUID_FD_ARG ATTRIBUTE_FD_ARG
# else
#  define DISFLUID_FD_ARG()
# endif

# define DISFLUID_FD_ARG_1 \
  DISFLUID_FD_ARG (1)

# ifdef ATTRIBUTE_FD_ARG_READ
#  define DISFLUID_FD_ARG_READ ATTRIBUTE_FD_ARG_READ
# else
#  define DISFLUID_FD_ARG_READ(i)
# endif

# define DISFLUID_FD_ARG_READ_1 \
  DISFLUID_FD_ARG_READ (1)

# define DISFLUID_FD_ARG_READ_2 \
  DISFLUID_FD_ARG_READ (2)

# ifdef ATTRIBUTE_FD_ARG_WRITE
#  define DISFLUID_FD_ARG_WRITE ATTRIBUTE_FD_ARG_WRITE
# else
#  define DISFLUID_FD_ARG_WRITE(i)
# endif

# define DISFLUID_FD_ARG_WRITE_1 \
  DISFLUID_FD_ARG_WRITE (1)

# define DISFLUID_FD_ARG_WRITE_2 \
  DISFLUID_FD_ARG_WRITE (2)

# ifdef __cplusplus
extern "C"
{
# endif				/* __cplusplus */

  /**
   * disfluid_version:
   *
   * Returns: the version number.
   */
  DISFLUID_CONST DISFLUID_API extern const char *disfluid_version (void);

  /**
   * disfluid_is_nightly:
   *
   * Returns: (type boolean): whether disfluid is an unreleased snapshot.
   */
  DISFLUID_CONST DISFLUID_API extern int disfluid_is_nightly (void);

  /**
   * disfluid_whats_new:
   *
   * Returns: return the news.
   */
  DISFLUID_API extern const char *disfluid_whats_new (void);

  /**
   * disfluid_major_version:
   *
   * Returns: return the major version above which everything is
   * considered new.
   */
  DISFLUID_CONST DISFLUID_API extern const char
    *disfluid_major_version (void);

  /**
   * disfluid_website:
   *
   * Returns: the website for disfluid.
   */
  DISFLUID_API extern const char *disfluid_website (void);

  /**
   * disfluid_run_tests:
   * @n_tests: (out): the number of tests that have been run.
   * @n_errors: (out): the number of tests that failed.
   *
   * Run the unit tests.
   *
   * Returns: (transfer full): the tests output.
   */
  DISFLUID_API DISFLUID_DEALLOC_WITH_FREE char
    *disfluid_run_tests (size_t *n_tests, size_t *n_errors);

  /**
   * disfluid_count_authors:
   *
   * Returns: the number of authors.
   */
  DISFLUID_CONST DISFLUID_API extern size_t disfluid_count_authors (void);

  /**
   * disfluid_author_name:
   * @i: the author index.
   *
   * Returns: the name of author @i.
   */
  DISFLUID_API extern const char *disfluid_author_name (size_t i);

  /**
   * disfluid_author_email:
   * @i: the author index.
   *
   * Returns: (nullable): the optional email address of author @i.
   */
  DISFLUID_API extern const char *disfluid_author_email (size_t i);

  /**
   * disfluid_author_uri:
   * @i: the author index.
   *
   * Returns: (nullable): the optional web address of author @i.
   */
  DISFLUID_API extern const char *disfluid_author_uri (size_t i);

  /**
   * disfluid_author_is_developer:
   * @i: the author index.
   *
   * Returns: (type boolean): whether @i is a developer.
   */
  DISFLUID_CONST DISFLUID_API extern int
    disfluid_author_is_developer (size_t i);

  /**
   * disfluid_author_is_designer:
   * @i: the author index.
   *
   * Returns: (type boolean): whether @i is a designer.
   */
  DISFLUID_CONST DISFLUID_API extern int
    disfluid_author_is_designer (size_t i);

  /**
   * disfluid_author_is_artist:
   * @i: the author index.
   *
   * Returns: (type boolean): whether @i is a artist.
   */
  DISFLUID_CONST DISFLUID_API extern int disfluid_author_is_artist (size_t i);

  /**
   * disfluid_author_is_documenter:
   * @i: the author index.
   *
   * Returns: (type boolean): whether @i is a documenter.
   */
  DISFLUID_CONST DISFLUID_API extern int
    disfluid_author_is_documenter (size_t i);

  /**
   * disfluid_translation_credits:
   *
   * Returns: the credits for translations of your language.
   */
  DISFLUID_API extern const char *disfluid_translation_credits (void);

  /**
   * disfluid_copyright_statement:
   *
   * Returns: a copyright statement in your language.
   */
  DISFLUID_API DISFLUID_DEALLOC_WITH_FREE extern char
    *disfluid_copyright_statement (void);

  /**
   * disfluid_compute_cache_key:
   * @request_scheme: the URI scheme for the request, such as 'https'.
   * @request_header: the request header.
   * @response_header: the header of the response that resulted from
   * the request.
   * @key: (array length=max_key) (element-type char): the destination
   * cache key.
   *
   * Returns: 0 on success, or a negative error number.
   */
  DISFLUID_API extern int
    disfluid_compute_cache_key (const char *request_scheme,
				const char *request_header,
				const char *response_header,
				size_t max_key, char *key);

  /**
   * disfluid_hash_primary_cache_key:
   * @method: the request method, such as 'GET'.
   * @uri: the request URI.
   * @password: (nullable) (array length=password_length)
   * (element-type char): an optional password. If set, the hash
   * cannot be figured out without it.
   * @hash: (array length=max_hash) (element-type char): the
   * destination hash.
   *
   * Returns: 0 on success, or a negative error number.
   */
  DISFLUID_API extern int
    disfluid_hash_primary_cache_key (const char *method,
				     const char *uri,
				     const char *password,
				     size_t password_length,
				     size_t max_hash, char *hash);

  /**
   * disfluid_greet_world:
   *
   * Returns: a greeting in the user’s language.
   */
  DISFLUID_API const char *disfluid_greet_world (void);

  /**
   * disfluid_metaphor_name:
   *
   * Returns: a GNOME metaphor name for the application.
   */
  DISFLUID_API const char *disfluid_metaphor_name (void);

  /**
   * disfluid_test_append_init:
   * @fd: (out): the opened file descriptor.
   * @filename: (out): the file name.
   *
   * Open a temporary file for the concurrent "append" test.
   *
   * Returns: 0 on success, a negative value on error.
   */
  DISFLUID_API int disfluid_test_append_init (int *fd, char **filename);

  /**
   * disfluid_test_append_hello:
   * @filename: a file name.
   * @n_before: the number of hellos before this call.
   * @n_after: the number of hellos after this call.
   *
   * Open @filename, and append "Hello!" to it.
   */
  DISFLUID_API int disfluid_test_append_hello (const char *filename,
					       size_t *n_before,
					       size_t *n_after);

  /**
   * disfluid_test_append_count:
   * @filename: a file name.
   * @n: (out): the number of occurences of "Hello!"
   *
   * Open @filename, check that it contains a whole sequence of
   * "Hello!", and count the number of occurences.
   */
  DISFLUID_API int disfluid_test_append_count (const char *filename,
					       size_t *n);

# ifdef __cplusplus
}
# endif				/* __cplusplus */

#endif				/* not H_DISFLUID_INCLUDED */
