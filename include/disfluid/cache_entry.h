#ifndef H_DISFLUID_CACHE_ENTRY_INCLUDED
# define H_DISFLUID_CACHE_ENTRY_INCLUDED

# include <disfluid.h>

# ifdef __cplusplus
extern "C"
{
# endif				/* __cplusplus */

  /**
   * disfluid_cache_entry: (rename-to DisfluidCacheEntry):
   *
   * One response header, body, sprinkled with metadata.
   */
  struct disfluid_cache_entry;

  /**
   * disfluid_cache_entry_size:
   * @max_key: the maximum number of bytes in the key.
   * @max_header: the maximum number of bytes in the response header.
   * @max_body: the maximum number of bytes in the response body.
   *
   * Compute the required memory size to hold an entire cache entry.
   *
   * Returns: the minimum number of bytes to allocate.
   */
  DISFLUID_API DISFLUID_CONST extern size_t
    disfluid_cache_entry_size (size_t max_key,
			       size_t max_header, size_t max_body);

  /**
   * disfluid_cache_entry_alignment:
   *
   * Compute the minimum alignment to hold a cache entry.
   *
   * Returns: the minimum alignment.
   */
  DISFLUID_API DISFLUID_CONST extern size_t
    disfluid_cache_entry_alignment (void);

  /**
   * disfluid_cache_entry_init:
   * @entry: (type DisfluidCacheEntry): an allocated memory region,
   * with a large enough size and alignment.
   * @max_key: the maximum number of bytes in the key.
   * @max_header: the maximum number of bytes in the response header.
   * @max_body: the maximum number of bytes in the response body.
   *
   * Prepare the cache entry.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_init (struct disfluid_cache_entry *entry,
			       size_t max_key,
			       size_t max_header, size_t max_body);

  /**
   * disfluid_cache_entry_minimum_size:
   * @entry: (type DisfluidCacheEntry): a valid cache entry.
   * @min_key: (out): the number of bytes in the key.
   * @min_header: (out): the number of bytes in the response header.
   * @min_body: (out): the number of bytes in the response body.
   *
   * Return the number of bytes used for the key, header and body.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_minimum_size (const struct disfluid_cache_entry
				       *entry, size_t *min_key,
				       size_t *min_header, size_t *min_body);

  /**
   * disfluid_cache_entry_copy:
   * @dest: (type DisfluidCacheEntry): an allocated cache entry.
   * @src: (type DisfluidCacheEntry): a different allocated cache entry.
   *
   * Try and copy @dest to @src. It can fail if @dest is too small.
   *
   * Returns: 0 on success, -1 if @dest does not have enough allocated
   * for the key, -2 for the header, -3 for the key and the header, -4
   * for the body, -5 for the key and the body, -6 for the header and
   * the body, -7 for the key, header and body.
   */
  DISFLUID_API extern int
    disfluid_cache_entry_copy (struct disfluid_cache_entry *restrict dest,
			       const struct disfluid_cache_entry *restrict
			       src);

  /**
   * disfluid_cache_entry_alloc: (constructor):
   * @max_key: the maximum number of bytes in the key.
   * @max_header: the maximum number of bytes in the response header.
   * @max_body: the maximum number of bytes in the response body.
   *
   * Allocate a new cache entry.
   *
   * Returns: (nullable) (type DisfluidCacheEntry): a new cache entry.
   */
  DISFLUID_NODISCARD DISFLUID_API
    extern struct disfluid_cache_entry
    *disfluid_cache_entry_alloc (size_t max_key,
				 size_t max_header, size_t max_body);

  /**
   * disfluid_cache_entry_dup: (constructor):
   * @entry: (type DisfluidCacheEntry): the entry to copy.
   *
   * Allocate a new cache entry as a copy of @entry, keeping the same
   * allocation limits.
   *
   * Returns: (nullable) (type DisfluidCacheEntry): a new cache entry.
   */
  DISFLUID_NODISCARD DISFLUID_API
    extern struct disfluid_cache_entry
    *disfluid_cache_entry_dup (const struct disfluid_cache_entry *entry);

  /**
   * disfluid_cache_entry_free: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to destroy.
   *
   * Destroy @entry.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_free (struct disfluid_cache_entry *entry);

  /**
   * disfluid_cache_entry_set_request_date: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @request_date: the request date.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_set_request_date (struct disfluid_cache_entry *entry,
					   const struct timespec
					   *request_date);

  /**
   * disfluid_cache_entry_set_response_date: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @response_date: the response date.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_set_response_date (struct disfluid_cache_entry
					    *entry,
					    const struct timespec
					    *response_date);

  /**
   * disfluid_cache_entry_set_invalidated:
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @invalidated: (type boolean): whether it has been artificially invalidated.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_set_invalidated (struct disfluid_cache_entry *entry,
					  int invalidated);

  /**
   * disfluid_cache_entry_invalidate:
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_invalidate (struct disfluid_cache_entry *entry);

  /**
   * disfluid_cache_entry_set_key:
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @key: the new key bytes.
   *
   * Returns: 0 if the @key fits, a negative value otherwise.
   */
  DISFLUID_API extern int
    disfluid_cache_entry_set_key (struct disfluid_cache_entry *entry,
				  const char *key);

  /**
   * disfluid_cache_entry_set_response_header:
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @header: the new response header bytes.
   *
   * Returns: 0 if the @header fits, a negative value otherwise.
   */
  DISFLUID_API extern int
    disfluid_cache_entry_set_response_header (struct disfluid_cache_entry
					      *entry, const char *header);

  /**
   * disfluid_cache_entry_set_response_body:
   * @entry: (type DisfluidCacheEntry): the entry to modify.
   * @body: (array length=body_length) (element-type char): the new
   * response body bytes.
   *
   * Returns: 0 if the @body fits, a negative value otherwise.
   */
  DISFLUID_API extern int
    disfluid_cache_entry_set_response_body (struct disfluid_cache_entry
					    *entry,
					    size_t body_length,
					    const char *body);

  /**
   * disfluid_cache_entry_get_request_date: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to query.
   * @date: the request date.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_get_request_date (const struct disfluid_cache_entry
					   *entry, struct timespec *date);

  /**
   * disfluid_cache_entry_get_response_date: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to query.
   * @date: the response date.
   */
  DISFLUID_API extern void
    disfluid_cache_entry_get_response_date (const struct disfluid_cache_entry
					    *entry, struct timespec *date);

  /**
   * disfluid_cache_entry_is_invalidated:
   * @entry: (type DisfluidCacheEntry): the entry to query.
   *
   * Returns: (type boolean): whether @entry has been artificially invalidated.
   */
  DISFLUID_API DISFLUID_PURE extern int
    disfluid_cache_entry_is_invalidated (const struct disfluid_cache_entry
					 *entry);

  /**
   * disfluid_cache_entry_get_key:
   * @entry: (type DisfluidCacheEntry): the entry to query.
   * @start: the number of bytes of the key to skip.
   * @key: (array length=max) (element-type char): the key bytes to fill.
   *
   * Returns: the total number of key bytes.
   */
  DISFLUID_API extern size_t
    disfluid_cache_entry_get_key (const struct disfluid_cache_entry *entry,
				  size_t start, size_t max, char *key);

  /**
   * disfluid_cache_entry_get_header:
   * @entry: (type DisfluidCacheEntry): the entry to query.
   * @start: the number of bytes of the header to skip.
   * @header: (array length=max) (element-type char): the header bytes to fill.
   *
   * Returns: the total number of response header bytes.
   */
  DISFLUID_API extern size_t
    disfluid_cache_entry_get_header (const struct disfluid_cache_entry *entry,
				     size_t start, size_t max, char *header);

  /**
   * disfluid_cache_entry_get_body:
   * @entry: (type DisfluidCacheEntry): the entry to query.
   * @start: the number of bytes of the body to skip.
   * @body: (array length=max) (element-type char): the body bytes to fill.
   *
   * Returns: the total number of response body bytes.
   */
  DISFLUID_API extern size_t
    disfluid_cache_entry_get_body (const struct disfluid_cache_entry *entry,
				   size_t start, size_t max, char *body);

  /**
   * disfluid_cache_entry_load: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to initialize.
   * @load_key: (type boolean): whether to actually load the key.
   * @load_header: (type boolean): whether to actually load the response header.
   * @load_body: (type boolean): whether to actually load the response body.
   * @read_impl: how to read data.
   * @skip: how to skip data.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_load (struct disfluid_cache_entry *entry,
			       int load_key,
			       int load_header,
			       int load_body,
			       ssize_t (*read_impl) (void *user_data,
						     void *buffer,
						     size_t max_size),
			       int (*skip) (void *context, size_t size),
			       void *context);

  /**
   * disfluid_cache_entry_read:
   * @entry: (type DisfluidCacheEntry): the entry to initialize.
   * @load_key: (type boolean): whether to actually load the key.
   * @load_header: (type boolean): whether to actually load the response header.
   * @load_body: (type boolean): whether to actually load the response body.
   * @fd: the file descriptor to use.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_FD_ARG_READ_2 DISFLUID_API
    extern int disfluid_cache_entry_read (struct disfluid_cache_entry *entry,
					  int load_key,
					  int load_header,
					  int load_body, int fd);

  /**
   * disfluid_cache_entry_fread: (skip):
   * @entry: (type DisfluidCacheEntry): the entry to initialize.
   * @load_key: (type boolean): whether to actually load the key.
   * @load_header: (type boolean): whether to actually load the response header.
   * @load_body: (type boolean): whether to actually load the response body.
   * @f: the file handle to use.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_fread (struct disfluid_cache_entry *entry,
				int load_key,
				int load_header, int load_body, FILE * f);

  /**
   * disfluid_cache_entry_save:
   * @entry: (type DisfluidCacheEntry): the entry to save.
   * @write_impl: how to write data.
   * @user_context: (closure write_impl): the closure data for the callback.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_save (const struct disfluid_cache_entry *entry,
			       ssize_t (*write_impl) (void *context,
						      const void *buffer,
						      size_t max_size),
			       void *user_context);

  /**
   * disfluid_cache_entry_write:
   * @entry: (type DisfluidCacheEntry): the entry to save.
   * @fd: the file descriptor to use.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_FD_ARG_WRITE_2 DISFLUID_API
    extern int disfluid_cache_entry_write (const struct disfluid_cache_entry
					   *entry, int fd);

  /**
   * disfluid_cache_entry_fwrite:
   * @entry: (type DisfluidCacheEntry): the entry to save.
   * @f: the file handle to use.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_fwrite (const struct disfluid_cache_entry *entry,
				 FILE * f);

# define DISFLUID_CACHE_ENTRY_ALLOC(ptr, max_key, max_header, max_body) \
  (ptr = disfluid_cache_entry_alloc (max_key, max_header, max_body),    \
   assert (sizeof (size_t) == sizeof (struct disfluid_cache_entry *)),  \
   size_t offset,                                                       \
   memcpy (&offset, &ptr, sizeof (size_t)),                             \
   assert (offset % disfluid_cache_entry_alignment () == 0),            \
   ptr == NULL ? -1 : 0)

# define DISFLUID_CACHE_ENTRY_FREE(ptr)    \
  { free (ptr); ptr = NULL; }

  /**
   * disfluid_cache_entry_describe_request_date:
   * @entry: (type DisfluidCacheEntry): the entry to describe.
   * @description: (out): a short phrase stating the request date.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_describe_request_date (const struct
						disfluid_cache_entry *entry,
						char **description);

  /**
   * disfluid_cache_entry_describe_response_date:
   * @entry: (type DisfluidCacheEntry): the entry to describe.
   * @description: (out): a short phrase stating the response date.
   *
   * Returns: 0 on success, or a negative error code.
   */
  DISFLUID_NODISCARD DISFLUID_API extern int
    disfluid_cache_entry_describe_response_date (const struct
						 disfluid_cache_entry *entry,
						 char **description);

# ifdef __cplusplus
}
# endif				/* __cplusplus */

#endif				/* not H_DISFLUID_CACHE_ENTRY_INCLUDED */
