#ifndef H_DISFLUID_GOBJECT_INCLUDED
# define H_DISFLUID_GOBJECT_INCLUDED

# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# include <glib.h>
# include <glib-object.h>

# if defined _WIN32 && !defined __CYGWIN__
#  define DISFLUID_G_DLL_MADNESS 1
# else
#  define DISFLUID_G_DLL_MADNESS 0
# endif

# if BUILDING_LIBDISFLUID_GOBJECT && HAVE_VISIBILITY
#  define DISFLUID_G_DLL_EXPORTED __attribute__((__visibility__("default")))
# elif BUILDING_LIBDISFLUID_GOBJECT && DISFLUID_G_DLL_MADNESS
#  define DISFLUID_G_DLL_EXPORTED __declspec(dllexport)
# elif DISFLUID_G_DLL_MADNESS
#  define DISFLUID_G_DLL_EXPORTED __declspec(dllimport)
# else
#  define DISFLUID_G_DLL_EXPORTED
# endif

# define DISFLUID_G_API \
  DISFLUID_G_DLL_EXPORTED

# ifdef __cplusplus
extern "C"
{
# endif				/* __cplusplus */

# ifdef __cplusplus
}
# endif				/* __cplusplus */

#endif				/* not H_DISFLUID_GOBJECT_INCLUDED */
