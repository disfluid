#include <config.h>
#include <disfluid.h>

int
main (int argc, char *argv[])
{
  (void) argc;
  (void) argv;
  /* This is just a test runner, do not call bindtextdomain () */
  size_t n_tests, n_failures;
  char *output = disfluid_run_tests (&n_tests, &n_failures);
  free (output);
  if (n_failures != 0)
    {
      return 1;
    }
  return 0;
}
