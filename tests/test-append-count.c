#include <config.h>
#include <disfluid.h>
#include <stdio.h>

int
main (int argc, char *argv[])
{
  /* This is just a test runner, do not call bindtextdomain () */
  if (argc != 2)
    {
      fprintf (stderr, "Usage: test-append-hello <test file>\n");
      return 1;
    }
  size_t n;
  if (disfluid_test_append_count (argv[1], &n) < 0)
    {
      return 1;
    }
  printf ("%lu\n", n);
  return 0;
}
