#include <config.h>
#include <disfluid.h>
#include <unistd.h>
#include <stdio.h>

int
main (int argc, char *argv[])
{
  /* This is just a test runner, do not call bindtextdomain () */
  (void) argc;
  (void) argv;
  int fd;
  char *filename;
  if (disfluid_test_append_init (&fd, &filename) < 0)
    {
      return 1;
    }
  close (fd);
  printf ("%s\n", filename);
  return 0;
}
