#!/bin/sh

if TEST_FILENAME=$(./libtool --mode=execute tests/test-append-init)
then
    for task in $(seq 1 32)
    do
        ./libtool --mode=execute tests/test-append-hello $TEST_FILENAME &
    done
else
    exit 1
fi

while true
do
    if COUNT=$(./libtool --mode=execute tests/test-append-count $TEST_FILENAME)
    then
        if test "x$COUNT" = "x32"
        then
            exit 0
        fi
    else
        exit 1
    fi
done
