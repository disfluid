#include <config.h>
#include <disfluid.h>
#include <stdio.h>

int
main (int argc, char *argv[])
{
  /* This is just a test runner, do not call bindtextdomain () */
  if (argc != 2)
    {
      fprintf (stderr, "Usage: test-append-hello <test file>\n");
      return 1;
    }
  size_t n_before, n_after;
  if (disfluid_test_append_hello (argv[1], &n_before, &n_after) < 0)
    {
      return 1;
    }
  printf ("%lu -> %lu\n", n_before, n_after);
  return 0;
}
