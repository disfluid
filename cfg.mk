old_NEWS_hash = f0fb7934a30c4e89f0468433bd9317c7
_makefile_at_at_check_exceptions = ' && !/INTL_MACOSX_LIBS/ && !/GSETTINGS_RULES/ && !/AM_V/ && !/AM_DEFAULT_V/'

generated_files = $(srcdir)/lib/*.[ch] $(srcdir)/desktop/*.desktop.in
_gl_translatable_string_re ?= \b(N?_|gettext) *\([^)"]*("|$$)|translatable="yes"|^Comment=|<description>|dn?gettext *\(PACKAGE, *"

#"
# The line above is to reset buggy syntax highlighting

VC_LIST_ALWAYS_EXCLUDE_REGEX ?= ^doc/.*\.png$$

.PHONY: push-updated-translations sc_git_authors_known sc_appstream_news_hash

push-updated-translations:
	@rm -rf translations-updated
	git clone $(srcdir) translations-updated
	(cd translations-updated && git checkout translations)
	cp $(srcdir)/po/*.po translations-updated
	(cd translations-updated && git add *.po && git commit -m"Update translations" && git push)
	@rm -rf translations-updated

list_authors = $(GIT) log --pretty=format:%an

sc_git_authors_known:
	@$(list_authors) \
	  | sort -u \
	  | while read line ; \
	    do $(GREP) "$$line" $(srcdir)/AUTHORS >/dev/null 2>&1 \
	      || (echo "$$line does not appear in AUTHORS." ; \
		  exit 1) ; \
	    $(GREP) "$$line" $(srcdir)/src/libdisfluid/disfluid-authors.h >/dev/null 2>&1 \
	      || (echo "$$line does not appear in src/libdisfluid/disfluid-authors.h." ; \
		  exit 1) ; \
	    done

sc_appstream_news_hash:
	@$(GREP) "$(NEWS_hash)" $(srcdir)/src/libdisfluid/disfluid-version.h >/dev/null 2>&1 \
	  || (echo "The appstream news in src/libdisfluid/disfluid-version.h should be updated to reflect the changes in NEWS. Once this is done, write the NEWS hash in the file: $(NEWS_hash)" ; \
	      exit 1)

find_mdate = $(GIT) log -1 --pretty=format:%ai $(srcdir)/doc/disfluid.texi
find_mdate_full = LANG=en_US.UTF-8 date -u --date="$$($(find_mdate))" '+%d %B %Y'
find_mdate_month = LANG=en_US.UTF-8 date -u --date="$$($(find_mdate))" '+%B %Y'

sc_manual_update_date:
	@if UPDATED=$$($(find_mdate_full)) ; then $(GREP) "^@set UPDATED $${UPDATED}\$$" $(srcdir)/doc/disfluid.texi > /dev/null 2>&1 || (echo "The manual was last updated $$UPDATED, please change the UPDATED variable definition." ; exit 1) ; fi
	@if UPDATED_MONTH=$$($(find_mdate_month)) ; then $(GREP) "^@set UPDATED-MONTH $${UPDATED_MONTH}\$$" $(srcdir)/doc/disfluid.texi > /dev/null 2>&1 || (echo "The manual was last updated $$UPDATED_MONTH, please change the UPDATED-MONTH variable definition." ; exit 1) ; fi
